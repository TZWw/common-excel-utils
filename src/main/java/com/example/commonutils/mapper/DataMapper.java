package com.example.commonutils.mapper;

import com.example.commonutils.baseMapper.MyBaseMapper;
import com.example.commonutils.domain.UserData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DataMapper extends MyBaseMapper<UserData> {
    List<UserData> selectUserData();
}
