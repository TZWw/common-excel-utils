# CommonExcelUtils

#### 介绍
SpringBoot万能的Excel导入、导出模板
此模板是在使用[若依权限管理框架](https://gitee.com/y_project/RuoYi-Cloud)时遇到的，非常的方便好用

#### 软件架构
使用springboot


#### 使用说明

1.  实体类：在需要导出的字段上加入@Excel注解，name为导出后Excel的表头

