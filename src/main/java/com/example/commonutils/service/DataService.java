package com.example.commonutils.service;

import com.example.commonutils.domain.UserData;
import java.util.List;


public interface DataService {
    List<UserData> selectUserData();

    List<UserData> userDataList();

    int importUserData(List<UserData> userDataList);
}
