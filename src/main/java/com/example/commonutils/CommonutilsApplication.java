package com.example.commonutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.example.commonutils.mapper")
public class CommonutilsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonutilsApplication.class, args);
    }

}
